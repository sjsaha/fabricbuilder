swagger: "2.0"

info:
  description: "The SampleAsset chaincode can read/write SampleAssets
    onto the blockchain and can expose these functions as REST API.
    THIS SAMPLE CODE MAY BE USED SOLELY AS PART OF THE TEST AND EVALUATION OF THE SAP CLOUD PLATFORM
    HYPERLEDGER SERVICE (THE “SERVICE”) AND IN ACCORDANCE WITH THE AGREEMENT FOR THE SERVICE.
    THIS SAMPLE CODE PROVIDED “AS IS”, WITHOUT ANY WARRANTY, ESCROW, TRAINING, MAINTENANCE, OR
    SERVICE OBLIGATIONS WHATSOEVER ON THE PART OF SAP."
  version: "1.0"
  title: "SampleAsset"

consumes:
  - application/json

parameters:
  id:
    name: id
    in: path
    description: ID of the SampleAsset
    required: true
    type: string
    maxLength: 64

definitions:
  sampleAsset:
    type: object
    properties:
      ID:
        type: string
      docType:
        type: string
      status:
        type: string
      creationDate:
        type: string
      temperature:
        type: string

  sampleAssetHistory:
    type: object
    properties:
      values:
        type: array
        items:
          type: object
          properties:
            timestamp:
              type: string
            sample:
              $ref: '#/definitions/sampleAsset'

  sampleAssetByIDSearchResult:
    type: object
    properties:
      values:
        type: array
        items:
          type: object
          properties:
            ID:
              type: string
            sample:
              $ref: '#/definitions/sampleAsset'

paths:

  /sampleAsset:
    get:
      operationId: readAllSamples
      summary: Read all (existing) SampleAssets
      produces:
      - application/json
      responses:
        200:
          description: OK
        500:
          description: Failed

    post:
      operationId: addNewSample
      summary: Adds a new SampleAsset
      consumes:
      - application/json
      parameters:
      - in: body
        name: newSampleAsset
        description: New SampleAsset
        required: true
        schema:
          $ref: '#/definitions/sampleAsset'
      responses:
        200:
          description: SampleAsset Written
        500:
          description: Failed

  /sampleAsset/update:
    post:
      operationId: updateSample
      summary: Update SampleAsset
      consumes:
      - application/json
      parameters:
      - in: body
        name: Update SampleAsset
        description: SampleAsset for Update
        required: true
        schema:
          $ref: '#/definitions/sampleAsset'
      responses:
        200:
          description: SampleAsset Updated
        500:
          description: Failed

  /sampleAsset/clear:

    delete:
      operationId: removeAllSamples
      summary: Remove all SampleAssets
      produces:
      - application/json
      responses:
        200:
          description: OK
        500:
          description: Failed

  /sampleAsset/{id}:

    get:
      operationId: readSample
      summary: Read SampleAsset by SampleAsset ID
      parameters:
      - $ref: '#/parameters/id'
      produces:
      - application/json
      responses:
        200:
          description: OK
        500:
          description: Failed
    delete:
      operationId: removeSample
      summary: Remove SampleAsset by SampleAsset ID
      parameters:
      - $ref: '#/parameters/id'
      produces:
      - application/json
      responses:
        200:
          description: OK
        500:
          description: Failed

  /sampleAsset/{id}/history:

    get:
      operationId: getSampleHistory
      summary: Return history of Sample by ID
      parameters:
      - $ref: '#/parameters/id'
      produces:
      - application/json
      responses:
        200:
          $ref: '#/definitions/sampleAssetHistory'
        400:
          description: Parameter Mismatch
        404:
          description: Not Found

  /sampleAsset/searchSamplesByID/{wildcard}:

    get:
      operationId: searchSamplesByID
      summary: Find samples by ID - supports wildcard search in the text strings
      description: Search for all matching IDs, given a (regex) value expression and return both the IDs and text. For example '^H.llo' will match any string starting with 'Hello' or 'Hallo'.
      parameters:
      - name: wildcard
        in: path
        description: Wildcard regular expression to match against texts
        required: true
        type: string
        maxLength: 64
      responses:
        200:
          $ref: '#/definitions/sampleAssetByIDSearchResult'
        500:
          description: Internal Server Error
