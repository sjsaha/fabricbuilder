var {dialog, app} = require('electron').remote;
var spawn = require("cross-spawn");
var genConfigUtil = require("../configUtil/config.js");
var deployer = require("../genUtil/deployer.js");
var ccVersionUpdater = require("../genUtil/ccVersionUpdater.js")
var genScriptName = "/generateBizNetForProject.sh";
var depScriptName = "/initialDeployBizNetForProject.sh";
var tstScriptName = "/scripts/runCleanAllAndSetup.sh"

function generateProject(projectName){
  if ( genConfigUtil.getProjectStatus(projectName) === "generated" ) {
    dialog.showMessageBox({
      type: "error",
      title: "Fabric Builder Generation",
      message: "Project is already generated.\n Please choose another project"
    });
    return;
  }
  var scriptPath = genConfigUtil.readScriptPathSetting();
  var script = scriptPath + genScriptName;
  var project = genConfigUtil.getProject(projectName);
  var child = spawn('bash', [script, projectName, project.generationProjectPath, scriptPath, project.modelParsedJSONFile], {
    cwd: scriptPath,
    env: process.env
  });

  child.stdout.on('data', function (data) {
    genConfigUtil.changeProjectStatus(projectName, "generated");
    dialog.showMessageBox({
      type:"info",
      title: "Fabric Builder Generation",
      message:"Business Network is generated at: " + data.toString()
    });
  });
  child.stderr.on('data', function (data) {
    dialog.showMessageBox({
      type:"error",
      title: "Fabric Builder Generation",
      message:"Error(s) Occurred: \n" + data.toString()
    });
    // dialog.showErrorDialog(data.toString());
  });
  child.on('close', function (code) {
    // Do Nothing for now..
    // app.dialogs.showInfoDialog("Finished: " + code);
  });

}

function deployProject(projectName, cfChannelDetails){
  var cStatus = genConfigUtil.getProjectStatus(projectName);
  if ( cStatus === "modeled" || cStatus === "new" ) {
    dialog.showMessageBox({
      type: "error",
      title: "Fabric Builder Generation",
      message: "Project is not ready for generation.\n Complete modeling / generation first.\n Please choose another project"
    });
    return;
  }
  if ( cStatus === "deployed" ) {
    var deployedChaincodes = genConfigUtil.getDeployedChaincodes(projectName);
    var msg = "It looks like your project is already deployed.";
    for ( var i = 0; i < deployedChaincodes.length; i++ ) {
      msg += "\nEntity: " + deployedChaincodes[i].entityName
            + "\nChaincode Version: " + deployedChaincodes[i].version;
    }
    msg += "\nDo you want to increment versions or choose a new project?"
    dialog.showMessageBox({
      type: "error",
      buttons: ["Increment Version","Cancel"],
      title: "Fabric Builder Generation",
      message: msg
    }, (response) => {
      if (response === 0 ) {  //Increment Versions
          ccVersionUpdater.incrementVersions(projectName);
          genConfigUtil.changeProjectStatus(projectName, "ready to re-deploy")
      } else if ( response === 1 ) { //Choose new project => do nothing!
        return;
      };
    });
    return;
  }
  deployer.deployChaincodes(projectName, genConfigUtil.getProject(projectName).generationProjectPath, cfChannelDetails)
    .then( function(result) {
      genConfigUtil.changeProjectStatus(projectName, "deployed");
      genConfigUtil.addDeployedChannelName(projectName, cfChannelDetails);
      genConfigUtil.addDeployedChaincodes(projectName, result);
      var message = "Deployed Chaincodes are: ";
      for ( var i = 0; i < result.length; i++ ) {
        message += "\nEntity Name: " + result[i].entityName + "\nChaincodeId: " + result[i].chaincodeId + "\nVersion: " + result[i].version;
      }
      dialog.showMessageBox({
        type:"info",
        title: "Fabric Builder Generation",
        message: message
      });
    })
  .catch( function(error) {
    if (cStatus === "ready to re-deploy") {
      genConfigUtil.changeProjectStatus(projectName, "deployed");
    } else {
      genConfigUtil.changeProjectStatus(projectName, "generated");
    }
    dialog.showMessageBox({
      type:"error",
      title: "Fabric Builder Generation",
      message:"ERROR: \n" + error
    });
  });
}

function generateSampleData(projectName) {
  if ( genConfigUtil.getProjectStatus(projectName) !== "deployed" ) {
    dialog.showMessageBox({
      type: "error",
      title: "Fabric Builder Generation",
      message: "Project is not yet deployed.\nSample data cannot be generated.\nPlease choose another project."
    });
    return;
  }
  var script = genConfigUtil.getProject(projectName).generationProjectPath + tstScriptName;
  var wdir = genConfigUtil.getProject(projectName).generationProjectPath + "/scripts";
  var child = spawn('bash', [script], {
    cwd: wdir,
    env: process.env
  });

  child.stdout.on('data', function (data) {
    dialog.showMessageBox({
      type:"info",
      title: "Fabric Builder Generation",
      message:"Test data for the business network is successfully generated!" + data.toString()
    });
  });
  child.stderr.on('data', function (data) {
    dialog.showMessageBox({
      type:"error",
      title: "Fabric Builder Generation",
      message:"Error(s) Occurred: \n" + data.toString()
    });
  });
  child.on('close', function (code) {
    // Do Nothing for now..
    // app.dialogs.showInfoDialog("Finished: " + code);
  });
}

function deploySetupUI(projectName) {
  if ( genConfigUtil.getProjectStatus(projectName) !== "deployed" ) {
    dialog.showMessageBox({
      type: "error",
      title: "Fabric Builder Setup UI",
      message: "Project is not yet deployed.\nSetup UI cannot be deployed.\nPlease choose another project."
    });
    return;
  }
  var wdir = genConfigUtil.getProject(projectName).generationProjectPath + "/scripts";
  var scriptName = wdir + "/deployCFUI.sh";
  // CAUTION:  Only for Mac!
  // TODO: Cross-Platform Support
  var script = 'tell application "Terminal" to do script '+'"bash '+scriptName+'"';
  var child = spawn('osascript', ["-e", script], {
    cwd: wdir,
    env: process.env
  });

  child.stdout.on('data', function (data) {
    dialog.showMessageBox({
      type:"info",
      title: "Fabric Builder Setup UI Deployment",
      message:"Switching to new Terminal window.\n Please check open terminal windows in background"
    });
  });
  child.stderr.on('data', function (data) {
    dialog.showMessageBox({
      type:"error",
      title: "Fabric Builder Setup UI Deployment",
      message:"Error(s) Occurred: \n" + data.toString()
    });
  });
  child.on('close', function (code) {
    // Do Nothing for now..
    // app.dialogs.showInfoDialog("Finished: " + code);
  });
}

exports.generateProject = generateProject;
exports.deployProject = deployProject;
exports.generateSampleData = generateSampleData;
exports.deploySetupUI = deploySetupUI;
